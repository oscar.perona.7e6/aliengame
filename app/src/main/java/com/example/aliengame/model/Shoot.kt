package com.example.aliengame.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.aliengame.R

class Shoot(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.dsiparo)
    val width = screenX / 10f
    val height = screenY / 15f
    var positionX = 0F
    var positionY = 1400f
    var positionShot = RectF()

    fun updateShot(){
        positionY -= 20

        positionShot.left = positionX
        positionShot.top = positionY
        positionShot.right = positionShot.left + width
        positionShot.bottom = positionShot.top + height
    }
    fun restartValues(){
        positionX = 0F
        positionY = 1400f
        positionShot = RectF()
    }
}