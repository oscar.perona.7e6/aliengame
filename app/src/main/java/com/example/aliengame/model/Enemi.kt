package com.example.aliengame.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.aliengame.R

class Enemi(context: Context, screenX: Int, screenY: Int,var positionY:Int,var positionX:Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.alien)
    val width = screenX / 10f
    val height = screenY / 15f
    val range = 10..15
    var speed = range.random()
    var positionEnemy = RectF()

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(), false)
    }

    fun updateEnemy(screenWidth: Int) {
        val listOfY = listOf<Int>(300, 500, 700, 900)
        positionX += speed
        if (positionX + width > screenWidth){
            positionX = (screenWidth-width).toInt()
            positionY = listOfY.random()
            speed =- speed
        }
        else if (positionX < 0){
            positionX = listOfY.random()
            positionY = 130
            speed =-speed
        }
        positionEnemy.left = positionX.toFloat()
        positionEnemy.top = positionY.toFloat()
        positionEnemy.right = positionEnemy.left + width
        positionEnemy.bottom = positionEnemy.top + height
    }
}