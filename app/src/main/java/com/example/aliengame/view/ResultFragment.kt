package com.example.aliengame.view

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.aliengame.R
import com.example.aliengame.databinding.FragmentMenuBinding
import com.example.aliengame.databinding.FragmentResultBinding

class ResultFragment : Fragment() {
    lateinit var binding: FragmentResultBinding
    lateinit var mediaPlayer: MediaPlayer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentResultBinding.inflate(layoutInflater)
        mediaPlayer = MediaPlayer.create(context, R.raw.background_music)
        mediaPlayer.start()
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mediaPlayer.setOnCompletionListener {
            mediaPlayer.start()
        }

        val lives = arguments?.getInt("lives").toString().toInt()

        if (lives>0){
            binding.result.text = "HAS GANADO, HAS ELIMINADO A TODOS LOS ENEMIGOS"
            binding.lives.text = "Vidas restantes: $lives"
        }
        else {
            binding.result.text = "HAS PERDIDO, TE HAS QUEDADO SIN VIDAS"
            binding.lives.visibility = View.INVISIBLE
        }

        binding.menuBttn.setOnClickListener {
            findNavController().navigate(R.id.action_resultFragment2_to_menuFragment)
        }

        binding.shareButton.setOnClickListener {

            if (lives > 0){
                val t1 = "Hola! Acabo de jugar a alienGame y me han sobrado $lives vidas!!!"
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, t1)
                startActivity(Intent.createChooser(shareIntent, "Share via"))
            }
            else{
                val t1 = "Hola! Acabo de perder en AlienGame...Tu crees que podrias ganar?"
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, t1)
                startActivity(Intent.createChooser(shareIntent, "Share via"))
            }

        }
    }
}