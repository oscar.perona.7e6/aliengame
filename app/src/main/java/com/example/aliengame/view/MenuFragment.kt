package com.example.aliengame.view

import android.graphics.Point
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.aliengame.R
import com.example.aliengame.databinding.FragmentMenuBinding

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding
    lateinit var mediaPlayer: MediaPlayer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)

        mediaPlayer = MediaPlayer.create(context, R.raw.background_music)
        mediaPlayer.start()
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mediaPlayer.setOnCompletionListener {
            mediaPlayer.start()
        }

        binding.gameButton.setOnClickListener {
            mediaPlayer.stop()
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment2)
        }
        binding.heltBttn.setOnClickListener{
            findNavController().navigate(R.id.action_menuFragment_to_helpFragment)
        }

    }

}