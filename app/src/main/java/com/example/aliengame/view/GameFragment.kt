package com.example.aliengame.view

import android.graphics.Color
import android.graphics.Point
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.example.aliengame.R

class GameFragment : Fragment() {

    private lateinit var gameView: GameView
    lateinit var fireButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val display = requireActivity().windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        gameView = GameView(requireContext(), size)
        gameView = GameView(requireContext(), size)
        val game: FrameLayout = FrameLayout(requireContext())
        val gameButtons: RelativeLayout = RelativeLayout(requireContext())
        fireButton= Button(requireContext());
        fireButton.setText("Fire")
        fireButton.setBackgroundColor(Color.RED)
        val b1 = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            RelativeLayout.LayoutParams.FILL_PARENT
        )
        gameButtons.setLayoutParams(params)
        gameButtons.addView(fireButton)
        b1.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)
        b1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        fireButton.setLayoutParams(b1)
        game.addView(gameView)
        game.addView(gameButtons)
        return game
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.laser)

        fireButton.setOnClickListener {
            mediaPlayer?.start()
            gameView.shot()
        }
    }

}