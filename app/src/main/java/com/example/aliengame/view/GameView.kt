package com.example.aliengame.view

import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.fragment.app.findFragment
import androidx.navigation.fragment.findNavController
import com.example.aliengame.R
import com.example.aliengame.model.Enemi
import com.example.aliengame.model.Player
import com.example.aliengame.model.Shoot
import kotlinx.coroutines.*

class GameView(context: Context, private val size: Point) : SurfaceView(context){
    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()
    var playing = true
    var shotAction = false
    val player = Player(getContext(), size.x, size.y)
    val shoot = Shoot(getContext(), size.x, size.y)
    var lvl = 1
    val listOfEnemy = mutableListOf<Enemi>()
    var lives = 3

    lateinit var backgroundImage:Bitmap
    private val mediaPlayer: MediaPlayer = MediaPlayer.create(context, R.raw.background_music)

    init {
        startGame()
        mediaPlayer.start()
    }
    fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
            mediaPlayer?.setOnCompletionListener {
                mediaPlayer?.start()
            }

            addEnemy()

            while(playing){
                draw()
                update()
                delay(10)
            }

            mediaPlayer.stop()
            val action = GameFragmentDirections.actionGameFragment2ToResultFragment2(lives)
            findFragment<GameFragment>().findNavController().navigate(action)
        }
    }

    fun addEnemy(){
        val rangeOfX = 0..width
        val listOfY = listOf<Int>(300, 500, 700, 900)
        repeat(5){
            listOfEnemy.add(Enemi(getContext(), size.x, size.y, listOfY.random(),rangeOfX.random()))
        }
    }

    fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()

            //SCORE
            paint.color = Color.YELLOW
            paint.textSize = 60f
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawColor(Color.BLACK)
            canvas.drawText("Lives: $lives", (size.x - paint.descent()), 75f, paint)

            //ENEMY
            val screenWidth = 0..width
            var ocupedPositions = mutableListOf<Int>()
            for (enemy in listOfEnemy){
                var printedEnemy = false
                while (!printedEnemy){
                    if (enemy.positionX !in ocupedPositions && enemy.positionX in screenWidth){
                        ocupedPositions.add(enemy.positionX)
                        canvas.drawBitmap(enemy.bitmap, enemy.positionX.toFloat(),enemy.positionY.toFloat(), paint)
                        printedEnemy = true
                    }
                    else if (enemy.positionX !in screenWidth){
                        enemy.positionX = screenWidth.random()
                    }
                    else{
                        enemy.positionX+=100
                    }
                }
                ocupedPositions = mutableListOf<Int>()
            }
            canvas.drawBitmap(player.bitmap, player.positionX.toFloat(),1550f, paint)
            if (shotAction){
                canvas.drawBitmap(shoot.bitmap, shoot.positionX,shoot.positionY, paint)
            }
            checkShotColision()
            // Aquí dibuixem els elements de la pantalla de joc
            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun update(){
        val screenWidth: Int = width
        for (enemy in listOfEnemy){
            enemy.updateEnemy(screenWidth)
        }
        player.updatePlayer()
        if(shotAction){
            shoot.updateShot()
        }
        if (shoot.positionY == -200F){
            lives--
            shoot.positionY = 1400F
            shotAction = false
        }

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when(event.action){
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    // Modifiquem la velocitat del jugador perquè es mogui?
                    if(event.x>player.positionX){
                        player.positionX += 15
                    }
                    else player.positionX -= 15
                }
            }
        }
        return true
    }

    fun shot(){
        shoot.positionX = player.positionX + player.width/2
        shotAction = true
    }

    fun checkShotColision(){
        for (enemi in listOfEnemy){
            if(RectF.intersects(enemi.positionEnemy, shoot.positionShot)){
                listOfEnemy.remove(enemi)
                shotAction = false
                shoot.restartValues()
                break
            }
        }
        gameStatus()
    }

    fun gameStatus(){
        if (listOfEnemy.isEmpty() && lvl <=3 && lives > 0){
            lvl++
            repeat(lvl){
                addEnemy()
            }
            drawLvl()
        }
        else if(lives == 0 || lvl == 4) {
            playing = false
        }
    }

    fun drawLvl(){
            if (holder.surface.isValid) {
                when(lvl){
                    1 -> backgroundImage = BitmapFactory.decodeResource(resources, R.drawable.lvl1)
                    2 -> backgroundImage = BitmapFactory.decodeResource(resources, R.drawable.lvl2)
                    3 -> backgroundImage = BitmapFactory.decodeResource(resources, R.drawable.lvl3)
                }
                canvas.drawBitmap(backgroundImage,0F,0F,null)
            }
        runBlocking{
            delay(3000)
        }
    }
}
